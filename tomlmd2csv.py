#!/usr/bin/python3
"""Convert Markdown to apps.csv

Usage:
  tomlmd2csv.py <path> <output_file>
  tomlmd2csv.py (-h | --help)

Options:
  -h --help     Show this screen.
"""
import pathlib
import re

import frontmatter
from docopt import docopt

import typedcsv


def map_metadata(record):
    return {
        "name": record.get("title", None),
        "summary": record.get("description", None),
        "reported_date": record.get("date", None),
        "updated_date": record.get("updated", None),
        "taxonomies": record.get("taxonomies", {}),
        "extra": record.get("extra", {}),
    }


with open("page.regex.txt", "r", encoding="utf-8") as f:
    PAGE_PATTERN = re.compile(f.read().replace("\n", ""), re.MULTILINE)


def process_file(filename: pathlib.Path):
    with open(filename, encoding="utf-8") as f:
        doc = frontmatter.load(f)
        record = map_metadata(doc.metadata)
        if match := PAGE_PATTERN.match(doc.content):
            record |= match.groupdict()
        return record


def run(path: pathlib.Path, output_file: pathlib.Path):
    data = []
    for filename in path.glob("**/*.md"):
        data += [process_file(filename)]
    typedcsv.write_csv(output_file, data)


if __name__ == "__main__":
    arguments = docopt(__doc__)
    args = {
        "path": pathlib.Path(arguments["<path>"]),
        "output_file": pathlib.Path(arguments["<output_file>"]),
    }
    run(**args)
