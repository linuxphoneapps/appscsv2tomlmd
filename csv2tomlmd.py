#!/usr/bin/python3
"""Convert apps.csv to Markdown

Usage:
  csv2tomlmd.py <filename> [<output_dir>]
  csv2tomlmd.py (-h | --help)

Options:
  -h --help     Show this screen.
"""
import datetime
import pathlib
import re
from typing import Optional

import frontmatter
import jinja2
from docopt import docopt

import typedcsv


def generate_filename(record):
    if record.get("extra", {}).get("app_id"):
        return record["extra"]["app_id"].lower() + ".md"

    m = re.match(r"^(?:https?://)(?P<domain>[^/]+)/(?P<path>.*)$", record["extra"]["repository"]).groupdict()
    path_name = ".".join([name.lower().replace("~", "").replace("_", "-") for name in m["path"].split("/") if name])
    return f"noappid.{path_name}.md"


def generate_metadata(record):
    return {
        "title": record.get("name", None),
        "description": record.get("summary", None),
        "aliases": record.get("aliases", {}),
        "date": datetime.date.fromisoformat(record["reported_date"]) if record.get("reported_date") else None,
        "updated": datetime.date.fromisoformat(record["updated_date"]) if record.get("updated_date") else None,
        "taxonomies": record.get("taxonomies", {}),
        "extra": record.get("extra", {}),
    }


def generate_page(record):
    env = jinja2.Environment(loader=jinja2.FileSystemLoader("."), autoescape=jinja2.select_autoescape())
    template = env.get_template("page.md.j2")
    return template.render(**record)


def generate_file(output_dir, record):
    doc = frontmatter.Post(content="")
    doc.metadata = generate_metadata(record)
    doc.content = generate_page(record)
    output_dir.mkdir(parents=True, exist_ok=True)
    filename = generate_filename(record)
    with open(output_dir / filename, "wb") as f:
        frontmatter.dump(doc, f, handler=frontmatter.default_handlers.TOMLHandler())


def run(filename: pathlib.Path, output_dir: Optional[pathlib.Path] = None):
    if output_dir is None:
        output_dir = pathlib.Path(filename.stem)
    for record in typedcsv.read_csv(filename):
        generate_file(output_dir, record)


if __name__ == "__main__":
    arguments = docopt(__doc__)
    args = {
        "filename": pathlib.Path(arguments["<filename>"]),
        "output_dir": pathlib.Path(arguments["<output_dir>"]) if arguments["<output_dir>"] else None,
    }
    run(**args)
