import csv


def parse_row(row, ignore_empty=False):
    result = {}
    for key, value in row.items():
        # ignore empty entries
        if ignore_empty and not value:
            continue

        # parse arrays (key: [name], value: v1,...)
        if len(key) >= 2 and key[0] == "[" and key[-1] == "]":
            key = key[1:-1]
            value = [s.strip() for s in value.split(",") if s.strip()]

        # hierarchial keys: row[a.b] -> row[a][b]
        keys = key.split(".")
        item = result
        for keypart in keys[:-1]:
            if keypart not in item:
                item[keypart] = {}
            item = item[keypart]
        item[keys[-1]] = value
    return result


def read_csv(path):
    with open(path, "r", encoding="utf-8", newline="") as f:  # why newline="": see footnote at the end of https://docs.python.org/3/library/csv.html
        reader = csv.DictReader(f)
        for row in reader:
            yield parse_row(row)


def flatten(data):
    if not isinstance(data, dict):
        raise TypeError("Top-level data must be of type dict")

    result = {}
    for key, value in data.items():
        if isinstance(value, dict):
            for k, v in flatten(value).items():
                is_array = k.startswith("[") and k.endswith("]")
                k = f"{key}.{k[1:-1] if is_array else k}"
                k = f"[{k}]" if is_array else k
                result[k] = v
        elif isinstance(value, (list, set, tuple)):
            result[f"[{key}]"] = ",".join(value)
        else:
            result[key] = value

    return result


def get_columns(data):
    columns = []
    for entry in data:
        columns += [column for column in entry.keys() if column not in columns]
    return columns


def write_csv(filename, data):
    flattened_data = [flatten(d) for d in data]
    with open(filename, "w", encoding="utf-8", newline="") as f:
        writer = csv.DictWriter(f, fieldnames=get_columns(flattened_data))
        writer.writeheader()
        for row in flattened_data:
            writer.writerow(row)
